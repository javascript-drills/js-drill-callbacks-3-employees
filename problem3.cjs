// 3. Get all data for company Powerpuff Brigade

const fs = require('fs');

function getAllDataOfCompany(data, companyName, callback){
    try{
        // console.log(data)
        let solutionObject = {};
        data.forEach( (index) => {
            if(index.company === companyName){
                if(solutionObject[index.company] === undefined){
                    solutionObject[index.company] = [];
                }
                solutionObject[index.company].push(index);
            }
        });
        // console.log(solutionObject);
        fs.writeFile("../outputs/3-output.json", JSON.stringify(solutionObject, null, 1), (err) => {
            try{
                if(err){
                    callback(`fs Error: ${err}`)
                }
                else{
                    callback(null, "Company data file written successfully.");
                }
            }catch(err){
                console.error(`Callback Error ${err}`);
            }
        });




    }catch(err){
        console.error(`Error in funtion ${err}`);
    }
}

module.exports = getAllDataOfCompany;