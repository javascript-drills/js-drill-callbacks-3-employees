// 2. Group data based on companies.
// { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}

const fs = require('fs');

function groupData(data, callback){
    try{
        // console.log(data)
        let solutionObject = {};

        data.forEach( (index) => {
            // console.log(index.company);
            if(solutionObject[index.company] === undefined){
                solutionObject[index.company] = [];
            }
            
            solutionObject[index.company].push(index);
        })
        // console.log(solutionObje);
        fs.writeFile("../outputs/2-output.json", JSON.stringify(solutionObject, null, 1), (err) => {
            try{
                if(err){
                    callback(`fs Error: ${err}`)
                }
                else{
                    callback(null, "Group data file written successfully.");
                }
            }catch(err){
                console.error(`Callback Error ${err}`);
            }
        });
    
    }catch(err){
        console.error(`Error in function ${err}`);
    }
}


module.exports = groupData;