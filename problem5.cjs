// 5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.

const fs = require('fs');

function getSortedData(data, callback){
    try{
        let solutionArray = JSON.parse(JSON.stringify(data));
        
        solutionArray.sort((val1, val2) => {
            if(val1.company !== val2.company){
                return (val1.company > val2.company) ? 1 : -1;
            }
            else{
                return val1.id-val2.id;
            }
        });
        // console.log(solutionArray);
        fs.writeFile("../outputs/5-output.json", JSON.stringify(solutionArray, null, 1), (err) => {
            try{
                if(err){
                    callback(`fs Error: ${err}`)
                }
                else{
                    callback(null, "Sorted data file written successfully.");
                }
            }catch(err){
                console.error(`Callback Error ${err}`);
            }
        });
        

    }catch(err){
        console.error(`Error in funtion ${err}`);
    }
}

module.exports = getSortedData;