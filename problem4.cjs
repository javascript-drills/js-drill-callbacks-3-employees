// 4. Remove entry with id 2.

const fs = require('fs');

function removeEntryOfId(data, companyId, callback){
    try{
        let solutionArray = data.filter( (index) => index.id !== companyId)

        // console.log(solutionArray);
        fs.writeFile("../outputs/4-output.json", JSON.stringify(solutionArray, null, 1), (err) => {
            try{
                if(err){
                    callback(`fs Error: ${err}`)
                }
                else{
                    callback(null, "Id entry removed and file written successfully.");
                }
            }catch(err){
                console.error(`Callback Error ${err}`);
            }
        });

    }catch(err){
        console.error(`Error in funtion ${err}`);
    }
}

module.exports = removeEntryOfId;