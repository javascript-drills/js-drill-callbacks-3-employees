// 1. Retrieve data for ids : [2, 13, 23].

const fs = require('fs');

function getDataFromId(data, id, callback){
    try{
        let retrievedArray = data.filter((index) => id.includes((index.id)));
        
        fs.writeFile("../outputs/1-output.json", JSON.stringify(retrievedArray, null, 1), (err)=>{
            try{
                if(err){
                    callback(`fs error: ${err}`);
                }
                else{
                    callback(null, "Ids data written successfully")
                }
            }catch(err){
                console.error(`Callback error: ${err}`);
            }
        });

    }catch(err){
        console.error(`Error in function ${err}`);
    }
}

module.exports = getDataFromId;