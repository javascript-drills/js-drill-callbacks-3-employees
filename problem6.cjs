// 6. Swap position of companies with id 93 and id 92.

const fs = require('fs');

function swapPositions(data, id1, id2, callback){
    try{
        // console.log(data)
        let solutionArray = JSON.parse(JSON.stringify(data));
        let index1 ;
        let  index2;

        data.forEach( (element ,index) => {
            if(element.id === id1){
                index1 = index;
            }
            else if(element.id === id2){
                index2 = index;
            }
        });
        // console.log(index2 ,index1)
        let temp = solutionArray[index1];
        solutionArray[index1] = solutionArray[index2];
        solutionArray[index2] = temp;

        // console.log(solutionArray);
        fs.writeFile("../outputs/6-output.json", JSON.stringify(solutionArray, null, 1), (err) => {
            try{
                if(err){
                    callback(`fs Error: ${err}`)
                }
                else{
                    callback(null, "Position swapped and file written successfully.");
                }
            }catch(err){
                console.error(`Callback Error ${err}`);
            }
        });


    }catch(err){
        console.error(`Error in funtion ${err}`);
    }
}

module.exports = swapPositions;