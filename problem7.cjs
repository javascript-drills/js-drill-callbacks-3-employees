// 7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

const fs = require('fs');

function addBirthdayByEvenId(data, callback){
    try{
        // console.log(data)
        let solutionArray = JSON.parse(JSON.stringify(data));
        let date = new Date;
        let currentDate = `${date.getDate()}-${date.getMonth()}-${date.getFullYear()}`;
        // console.log(currentDate);

        solutionArray.forEach((element) => {
            // console.log(element.id%2)
            if(element.id%2 == 0){
                // console.log(element);
                element.birthday = currentDate;
                // console.log(element)
            }
        });
        // console.log(solutionArray);
        fs.writeFile("../outputs/7-output.json", JSON.stringify(solutionArray, null, 1), (err) => {
            try{
                if(err){
                    callback(`fs Error: ${err}`)
                }
                else{
                    callback(null, "Added birthdays for even id employees and file written successfully.");
                }
            }catch(err){
                console.error(`Callback Error ${err}`);
            }
        });


    }catch(err){
        console.error(`Error in funtion ${err}`);
    }
}

module.exports = addBirthdayByEvenId;