const data = require("../data.json");
const getDataFromId = require("../problem1.cjs");
const groupData = require("../problem2.cjs")
const getAllDataOfCompany = require("../problem3.cjs")
const removeEntryOfId = require("../problem4.cjs")
const getSortedData = require("../problem5.cjs");
const swapPositions = require("../problem6.cjs")
const addBirthdayByEvenId = require("../problem7.cjs")

const dataArray = data.employees;
const ids = [2, 13, 23];


getDataFromId(dataArray, ids, (err, result) => {
    if(err){
        console.log(err);
    }
    else{
        console.log(result);
        //////////////////////////////////// 1
        groupData(dataArray, (err, result2) => {
            if(err){
                console.log(err);
            }
            else{
                console.log(result2);
                /////////////////////////////////////// 2
                getAllDataOfCompany(dataArray, "Powerpuff Brigade", (err, result3) => {
                    if(err){
                        console.log(err);
                    }
                    else{
                        console.log(result3);
                        ////////////////////////////////////3
                        removeEntryOfId(dataArray, 2, (err, result4) => {
                            if(err){
                                console.log(err);
                            }
                            else{
                                console.log(result4);
                                /////////////////////////////////4
                                getSortedData(dataArray, (err, result5) => {
                                    if(err){
                                        console.log(err);
                                    }
                                    else{
                                        console.log(result5);
                                        //////////////////////////////5
                                        swapPositions(dataArray, 92, 93, (err, result6) => {
                                            if(err){
                                                console.log(err);
                                            }
                                            else{
                                                console.log(result6);
                                                ///////////////////////////////6
                                                addBirthdayByEvenId(dataArray, (err, result7) => {
                                                    if(err){
                                                        console.log(err);
                                                    }
                                                    else{
                                                        console.log(result7);
                                                    }
                                                    /////////////////////////////////7
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
})